# README #

This repository contains the core libraries for the SophiaScript language.

### What is this repository for? ###

* Quick summary
* 0.0.1-pre-release
* [SophiaScript](https://sophiascript.com/)

### Core Library Modules ###

* Core
* Core Collections
* Core.View
* Core.Model
* Core.Session
* Core.Runtime
* Core.Security
* Core.Speech
* Core.Text

### Who do I talk to? ###

* Michael Goodwin mikejg101@icloud.com